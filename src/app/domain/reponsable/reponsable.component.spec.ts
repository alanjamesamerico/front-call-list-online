import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReponsableComponent } from './reponsable.component';

describe('ReponsableComponent', () => {
  let component: ReponsableComponent;
  let fixture: ComponentFixture<ReponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
