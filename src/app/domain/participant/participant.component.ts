import { Component, OnInit } from '@angular/core';
import { ParticipantService } from './participant.service';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css']
})
export class ParticipantComponent implements OnInit {

  participants: string[];

  constructor(private service: ParticipantService) { }

  ngOnInit(): void {
    this.participants = this.service.getAllParticipants();
  }
}
