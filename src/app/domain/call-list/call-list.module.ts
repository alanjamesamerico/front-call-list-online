import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CallListComponent } from './call-list.component';
import { IndexModule } from '../../index/index.module';


@NgModule({
  declarations: [
    CallListComponent
  ],
  imports: [
    CommonModule,
    IndexModule
  ],
  exports: [
    CallListComponent
  ],
  providers: []
})
export class CallListModule { }
