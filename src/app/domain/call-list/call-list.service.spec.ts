import { TestBed } from '@angular/core/testing';

import { CallListService } from './call-list.service';

describe('CallListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CallListService = TestBed.get(CallListService);
    expect(service).toBeTruthy();
  });
});
