import { CallListService } from './call-list.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-call-list',
  templateUrl: './call-list.component.html',
  styleUrls: ['./call-list.component.css']
})
export class CallListComponent implements OnInit {

  callListTile: string;

  constructor(private callListService: CallListService) {
    this.callListTile = this.callListService.getCallListTitle();
  }
    
  ngOnInit(): void {
    //throw new Error("Method not implemented.");
  }
}
