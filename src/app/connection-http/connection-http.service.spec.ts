import { TestBed } from '@angular/core/testing';

import { ConnectionHttpService } from './connection-http.service';

describe('ConnectionHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConnectionHttpService = TestBed.get(ConnectionHttpService);
    expect(service).toBeTruthy();
  });
});
