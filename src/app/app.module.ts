import { SidenavComponent } from './index/sidenav/sidenav.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { IndexModule } from './index/index.module';
import { CallListModule } from './domain/call-list/call-list.module';
import { AppComponent } from './app.component';
import { ParticipantComponent } from './domain/participant/participant.component';
import { ReponsableComponent } from './domain/reponsable/reponsable.component';
import { LoginComponent } from './login/login.component';
import { routing } from './index/app.routing';
import { HomeComponent } from './home/home.component';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [
    AppComponent,
    ParticipantComponent,
    ReponsableComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CallListModule,
    IndexModule,
    routing,
    MatExpansionModule
  ],
  bootstrap: [
    AppComponent
  ],
  providers: []
})
export class AppModule { }
