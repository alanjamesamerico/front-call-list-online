import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavMenuParticipantComponent } from './sidenav-menu-participant.component';

describe('SidenavMenuParticipantComponent', () => {
  let component: SidenavMenuParticipantComponent;
  let fixture: ComponentFixture<SidenavMenuParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavMenuParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavMenuParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
