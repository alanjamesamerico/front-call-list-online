import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavMenuCallListComponent } from './sidenav-menu-call-list.component';

describe('SidenavMenuCallListComponent', () => {
  let component: SidenavMenuCallListComponent;
  let fixture: ComponentFixture<SidenavMenuCallListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavMenuCallListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavMenuCallListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
