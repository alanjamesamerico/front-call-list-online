import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavMenuResponsableComponent } from './sidenav-menu-responsable.component';

describe('SidenavMenuResponsableComponent', () => {
  let component: SidenavMenuResponsableComponent;
  let fixture: ComponentFixture<SidenavMenuResponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavMenuResponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavMenuResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
