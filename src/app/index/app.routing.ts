import { LoginComponent } from './../login/login.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParticipantComponent } from '../domain/participant/participant.component';
import { ReponsableComponent } from '../domain/reponsable/reponsable.component';
import { CallListComponent } from '../domain/call-list/call-list.component';
import { IndexComponent } from './index.component';

const INDEX_ROUTES: Routes = [
    { path: 'call-list',   component: CallListComponent },
    { path: 'participant', component: ParticipantComponent },
    { path: 'responsable', component: ReponsableComponent },
    { path: 'home', component: IndexComponent},
    { path: '', component: LoginComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(INDEX_ROUTES);