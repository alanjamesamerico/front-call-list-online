import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';

import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { IndexComponent } from './index.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SidenavMenuComponent } from './sidenav/sidenav-menu/sidenav-menu.component';
import { SidenavMenuParticipantComponent } from './sidenav/sidenav-menu/sidenav-menu-participant/sidenav-menu-participant.component';
import { SidenavMenuCallListComponent } from './sidenav/sidenav-menu/sidenav-menu-call-list/sidenav-menu-call-list.component';
import { SidenavMenuResponsableComponent } from './sidenav/sidenav-menu/sidenav-menu-responsable/sidenav-menu-responsable.component';

@NgModule({
  declarations: [
    IndexComponent,
    NavbarComponent,
    SidenavComponent,
    SidenavMenuComponent,
    SidenavMenuParticipantComponent,
    SidenavMenuCallListComponent,
    SidenavMenuResponsableComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatGridListModule,
    MatSidenavModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    AppRoutingModule
  ],
  providers: [
  ],
  exports: [
    IndexComponent,
    SidenavMenuComponent
  ]
})
export class IndexModule {
}
